const express = require("express");
const path = require('path');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

//Init app
const app = express();

const Tarefa = require('./models/tarefa.js');
const Estados = require('./models/estados.js');
const Importancia = require('./models/importancia.js');
const Urgencia = require('./models/urgencia.js');
const Categoria =  require('./models/categoria.js');

let tarefas = [
    new Tarefa(1,"Estender a roupa", Estados.P, "Estender a roupa na cozinha", Urgencia.U, Importancia.I, 10, 0, Categoria.T, ""), 
    new Tarefa(2,"Ir comprar carne", Estados.P,"Ir ao talho X buscar a carne que foi encomendada de manhã. Não esquecer do dinheiro.", Urgencia.N, Importancia.I, 30, 0, Categoria.T, null)
];

// Load View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', function(req, res){
    
    /*Tarefa.find({}, function(err, tarefas){
        if(err){
            console.log(err);
        }else{*/
            res.render('index',{
            tituloAdicionar: 'Adicionar tarefa',
            tituloLista: 'Tarefas Pendentes',
            tituloConcluidas: 'Tarefas concluídas',
            ideiasProjetos: 'Ideias/Projetos',
            tarefas: tarefas
        });
        //}
    //});
});

//Add Route
app.get('/tarefas/add', function(req, res){
    res.render('add_todo',{
        titulo:'Adicionar tarefa'
    });
});

// Add Submit POST Route
app.post('/tarefas/add', function(req, res){
    let tarefa;// = new Tarefa();
    tarefa.nome = req.body.nome;
    tarefa.estadoDaTarefa = req.body.estadoDaTarefa = "Pendente";
    tarefa.descricaoDetalhada=req.body.descricaoDetalhada;
    tarefa.importancia=req.body.importancia;
    tarefa.urgencia=req.body.urgencia;
    tarefa.prioridade=req.body.prioridade;
    tarefa.tempoMinimo=req.body.tempoMinimo;
    tarefa.tempoUtilizado=req.body.tempoUtilizado;
    tarefa.categoria=req.body.categoria;
    tarefa.subcategoria=req.body.subcategoria;

    tarefa.save(function(err){
        if(err){
            console.log(err);
            return;
        }else{
            res.redirect('/');
        }
    });
});

//Get Single To-do list Route
app.get('/tarefas/:id', function (req, res){
    Tarefa.findById(req.params.id, function(err, tarefa){
        res.render('tarefa', {
            tarefa:tarefa
        });
    });
});

//Start Server
app.listen(8080, function(){
    console.log('Servidor iniciou na porta 8080...');
});