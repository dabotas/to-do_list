const Estados = require('./estados.js');
const Importancia = require('./importancia.js');
const Urgencia = require('./urgencia.js');
const Categoria =  require('./categoria.js');

class Tarefa{
    constructor(id, nome, estadoDaTarefa, descricaoDetalhada, eUrgente, eImportante, tempoMinimo, tempoUtilizado, categoria, subcategoria){
        this.id = id;
        this.nome = nome;
        this.estadoDaTarefa = estadoDaTarefa ? estadoDaTarefa : Estados.P;
        this.descricaoDetalhada = descricaoDetalhada ? descricaoDetalhada : "";
        this.eUrgente = eUrgente ? eUrgente : Urgencia.N;
        this.eImportante = eImportante ? eImportante : Importancia.N;
        this.tempoMinimo = tempoMinimo ? tempoMinimo : 30;
        this.tempoUtilizado = tempoUtilizado ? tempoUtilizado : 30;
        this.categoria = categoria ? categoria : Categoria.T;
        this.subcategoria = subcategoria ? subcategoria : "";
    }
    getPrioridade(){
        if(this.eImportante == Importancia.I && this.eUrgente == Urgencia.U){
            return 1;//Crítica - tarefas que devem ser feitas imediatamente
        }else if(this.eImportante == Importancia.I && this.eUrgente == Urgencia.N){
            return 2;//Alta - tarefas que você vai marcar para fazer depois
        }else if(this.eImportante == Importancia.N && this.eUrgente == Urgencia.U){
            return 3;//Média - tarefas que você vai delegar para outras pessoas ou pode fazer quando as que têm prioridade 1 e 2 forem concluídas
        }else
            return 4;//Baixa - tarefas que você vai riscar da sua lista ou pode considerar como projetos a fazer no tempo livre
    }
}

module.exports = Tarefa;